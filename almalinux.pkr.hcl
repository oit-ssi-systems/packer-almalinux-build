packer {
  required_version = ">= 1.7.0"
  required_plugins {
    vsphere = {
      version = ">= 1.2.7"
      source  = "github.com/hashicorp/vsphere"
    }
    ansible = {
      version = ">= 1.1.1"
      source  = "github.com/hashicorp/ansible"
    }
  }
}

variable "ansible_python_interpreter" {
  type    = string
  default = "/usr/bin/python"
}

variable "build_hash" {
  type    = string
  default = "${env("BUILD_HASH")}"
}

variable "build_hash_short" {
  type    = string
  default = "${env("BUILD_HASH_SHORT")}"
}

variable "build_os" {
  type    = string
  default = "${env("BUILD_OS")}"
}

variable "build_output" {
  type    = string
  default = "${env("BUILD_OUTPUT")}"
}

variable "installer-iso-datastore" {
  type    = string
  default = "${env("INSTALLER_ISO_DATASTORE")}"
}

variable "installer-proxy" {
  type    = string
  default = "http://proxy.oit.duke.edu:3128"
}

variable "ks-iso-datastore" {
  type    = string
  default = "${env("KS_ISO_DATASTORE")}"
}

variable "packer_root" {
  type    = string
  default = "${env("PACKER_ROOT_DIR")}"
}

variable "root_password" {
  type    = string
  default = "${env("TEMPLATE_PASSWORD")}"
}

variable "template-info-file" {
  type    = string
  default = "/etc/duke/template-info.txt"
}

variable "vsphere_cluster" {
  type    = string
  default = "${env("GOVC_CLUSTER")}"
}

variable "vsphere_datacenter" {
  type    = string
  default = "${env("GOVC_DATACENTER")}"
}

variable "vsphere_datastore" {
  type    = string
  default = "${env("GOVC_DATASTORE")}"
}

variable "vsphere_folder" {
  type    = string
  default = "${env("GOVC_FOLDER")}"
}

variable "vsphere_network" {
  type    = string
  default = "${env("GOVC_NETWORK")}"
}

variable "vsphere_password" {
  type    = string
  default = "${env("GOVC_PASSWORD")}"
}

variable "vsphere_resource_pool" {
  type    = string
  default = "${env("GOVC_RESOURCE_POOL")}"
}

variable "vsphere_server" {
  type    = string
  default = "${env("GOVC_URL")}"
}

variable "vsphere_username" {
  type    = string
  default = "${env("GOVC_USERNAME")}"
}

variable "template-os-version" {
  type = string
}

variable "template-vmware-hardware-version" {
  type    = string
  default = 15
}

variable "vmware-guest-id" {
  type = string
}

variable "installer-iso" {
  type = string
}

variable "installer-iso-url" {
  type = string
}

variable "installer-iso-checksum-url" {
  type = string
}

variable "installer-volume-label" {
  type = string
}

variable "content-library" {
  type    = string
  default = "${env("GOVC_CONTENT_LIBRARY")}"
}

variable "final-template-name" {
  type    = string
  default = "${env("FINAL_TEMPLATE_NAME")}"
}

source "vsphere-iso" "almalinux" {
  CPU_hot_plug         = true
  CPU_limit            = -1
  CPUs                 = 2
  RAM                  = "4096"
  RAM_hot_plug         = true
  boot_command         = ["c<wait>setparams 'packer-build'<enter>", "linuxefi /images/pxeboot/vmlinuz inst.stage2=hd:LABEL=${var.installer-volume-label} text biosdevname=0 inst.cmdline inst.proxy=${var.installer-proxy} net.ifnames=0 ip=dhcp<enter>", "initrdefi /images/pxeboot/initrd.img<enter><wait>", "boot<enter>"]
  boot_wait            = "15s"
  cluster              = "${var.vsphere_cluster}"
  datacenter           = "${var.vsphere_datacenter}"
  datastore            = "${var.vsphere_datastore}"
  disk_controller_type = ["pvscsi"]
  firmware             = "efi"
  folder               = "${var.vsphere_folder}"
  guest_os_type        = "${var.vmware-guest-id}"
  insecure_connection  = "true"
  iso_urls = [
    "${var.installer-iso-url}"
  ]
  iso_checksum = "file:${var.installer-iso-checksum-url}"
  iso_paths = [
    "[${var.ks-iso-datastore}]${var.build_os}-kickstart.iso"
  ]
  network_adapters {
    network      = "${var.vsphere_network}"
    network_card = "vmxnet3"
  }
  password         = "${var.vsphere_password}"
  resource_pool    = "${var.vsphere_resource_pool}"
  shutdown_command = "systemctl poweroff"
  ssh_password     = "${var.root_password}"
  ssh_private_key_file = "~/.ssh/id_ed25519"
  ssh_timeout      = "20m"
  ssh_username     = "root"
  storage {
    disk_size = 51200
  }
  username       = "${var.vsphere_username}"
  vcenter_server = "${var.vsphere_server}"
  vm_name        = "packer-${var.template-os-version}-${var.build_hash_short}"
  vm_version     = "${var.template-vmware-hardware-version}"

  content_library_destination {
    name    = var.final-template-name
    library = var.content-library
    ovf     = true
    destroy = true
  }

}

# a build block invokes sources and runs provisioning steps on them. The
# documentation for build blocks can be found here:
# https://www.packer.io/docs/templates/hcl_templates/blocks/build
build {
  sources = ["source.vsphere-iso.almalinux"]

  provisioner "shell" {
    environment_vars = ["TEMPLATE_INFO_FILE=${var.template-info-file}", "BUILD_HASH=${var.build_hash}", "BUILD_HASH_SHORT=${var.build_hash_short}", "BUILD_OS=${var.build_os}"]
    script           = "/scripts/template-info.sh"
  }

  provisioner "shell" {
    environment_vars = ["BUILD_OS=${var.build_os}", "http_proxy=http://proxy.oit.duke.edu:3128"]
    script           = "/scripts/install-ansible.sh"
  }

  provisioner "shell" {
    script = "/scripts/guest-cleanup.sh"
  }

  provisioner "ansible" {
    extra_arguments = ["-e","ansible_python_interpreter=${var.ansible_python_interpreter}","--scp-extra-args", "'-O'"]
    playbook_file   = "playbooks/packer-provisioning/packer-almalinux.yaml"
  }

  provisioner "shell" {
    inline = ["if [[ -e /etc/sysconfig/rhn/systemid ]]; then", "  rm -v /etc/sysconfig/rhn/systemid", "fi"]
  }

  post-processor "manifest" {
    output     = "${var.build_output}/manifest.json"
    strip_path = true
  }
}
