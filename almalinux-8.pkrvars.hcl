template-os-version              = "almalinux-8-x86_64"
vmware-guest-id                  = "centos8_64Guest"
installer-volume-label           = "AlmaLinux-8-10-x86_64-dvd"
installer-iso                    = ""
installer-iso-url                = "https://archive.linux.duke.edu/almalinux/8/isos/x86_64/AlmaLinux-8.10-x86_64-dvd.iso"
installer-iso-checksum-url       = "https://archive.linux.duke.edu/almalinux/8/isos/x86_64/CHECKSUM"
ansible_python_interpreter       = "/usr/bin/python3"
template-vmware-hardware-version = "19"